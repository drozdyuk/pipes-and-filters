import zmq
import time
import sys

def order_acc_endpoint(port="5556"):
    """Generates a total of 10 unique orders, and 5 duplicates"""
    def generate_message(num):
        message = "(security)(encryption) Hello world %s" % num
        print "-> User Entered: %s" % message
        socket.send (message)

    context = zmq.Context()
    print "Order Acc Endpoint: Connecting to server with port %s" % port
    socket = context.socket(zmq.PUSH)
    socket.connect("tcp://127.0.0.1:%s" % port)

    for i in range (5):
        generate_message(i + 1)
        time.sleep(1)

    for i in range(10):
        generate_message(i + 1)
        time.sleep (1)
