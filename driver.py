"""
Pipes and Filters Protocol
==========================

The pattern consists of three elements:

1. Sources
------------
Supplies messages to the system, possibly in response to user actions.

Sources are clients (connect) for the outoing messages service (PUSH).

2. Filter
----------
Accepts message on one end, and emits them at the other, usually doing
some transformation in-between.

Filters are "servers" (bind) for incoming messages (PULL) and clients (connect)
for the outgoing messages service (PUSH).

3. Sinks
---------
Accept messages and process them, possibly invoking other services.

Sinks are "servers" (bind) for incoming messages (PULL).


Andriy Drozdyuk, August 5, 2015.
"""

from  multiprocessing import Process
from order_acc_endpoint import order_acc_endpoint
from deduplicator import deduplicator
from decryptor import decryptor
from authenticator import authenticator
from order_placing_service import order_placing_service
from ports import *

if __name__ == "__main__":
    """
    Filter-chain we are constructing:
    order_acc_endpoint -> authenticator -> decryptor -> deduplicator -> order_placing_service
    """

    # Source
    Process(target=order_acc_endpoint, args=(authenticator_port,)).start()

    # Filters
    Process(target=authenticator, args=(authenticator_port, decryptor_port)).start()
    Process(target=decryptor, args=(decryptor_port, deduplicator_port)).start()
    Process(target=deduplicator, args=(deduplicator_port, order_placing_service_port)).start()

    # Sink
    Process(target=order_placing_service, args=(order_placing_service_port,)).start()
